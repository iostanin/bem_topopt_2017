#ifndef MATVEC_H
#define MATVEC_H

#include <iostream>
#include <vector>
#include <cmath>
#include <mpi.h>
#include <omp.h>
#include <petscksp.h>

//#define DIRECT_VOL true
//#define DIRECT_SUR true

using dvec = std::vector<double>;
using ivec = std::vector<int>;



void matvec(
             const PetscScalar * v,   // Input
             PetscScalar * Av,  // Output
             const int v_size,  // Size of v and Av

              // Additional parameters

             // Triangles
             const dvec & vert_1,
             const dvec & vert_2,
             const dvec & vert_3,

             // Boundary conditions
             const ivec & bc_t,

            // FMM args
            int mult_order,
            MPI_Comm comm
        );



void init_matvec(

             // Triangles
             const dvec & vert_1,
             const dvec & vert_2,
             const dvec & vert_3,

             // Boundary conditions
             const ivec & bc_t,

            // FMM args
            int mult_order,
            MPI_Comm comm
        );


void rhs(   dvec & v,   // Input
            dvec & Av,  // Output

             // Additional parameters
             // Triangles
             const dvec & vert_1,
             const dvec & vert_2,
             const dvec & vert_3,

             // Boundary conditions
             const ivec & bc_te,

             // FMM args
             int mult_order,
             MPI_Comm comm
        );

// This function subtracts inaccurate contributions
// from a current triangle, and then replaces them with
// exact analytical singular integral

void local_pass(

        dvec & subtr,
        dvec & add,

        dvec & v_1,
        dvec & v_2,
        dvec & v_3,

        int bc,
        dvec & val);

// This function generates the set of sources on the triangle
void triangle2src(
        int mode, // 1 - generate src, 2 - generate surf
        dvec & v_1,
        dvec & v_2,
        dvec & v_3,


        dvec & src_coord,
        dvec & src_value,
        dvec & val
        );

// This function generates the set of sources on the triangle
// with variable depth
void triangle2srcd(
        int mode, // 1 - generate src, 2 - generate surf
        int depth,
        dvec & v_1,
        dvec & v_2,
        dvec & v_3,

        dvec & src_coord,
        dvec & src_value,

        dvec & val
        );

#endif // MATVEC_H

// End of the file

void volume_matvec(

            int depth,
            // Triangles
            const dvec & vert_1,
            const dvec & vert_2,
            const dvec & vert_3,

            // Volume points
            dvec & trg_coord,

            // Surface solution
            const dvec & tractions,
            const dvec & displacements,

            dvec & stresses,

            // FMM args
            int mult_order,
            MPI_Comm comm
        );

void calculate_stresses2(
        dvec & src_coord,
        dvec & src_value,
        dvec & surf_coord,
        dvec & surf_value,
        dvec & trg_coord,
        dvec & trg_value,
        int mult_order,
        MPI_Comm comm);
