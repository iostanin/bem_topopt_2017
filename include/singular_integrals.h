#ifndef SINGULAR_INTEGRALS_H
#define SINGULAR_INTEGRALS_H
# define M_PI           3.14159265358979323846
#include <iostream>
#include <vector>
#include <cmath>

// Functions that compute singular integrals over triangles
// Used by local pass functions

using dvec = std::vector<double>;

///////////////////////////////////////////////////////////////////////////////

inline double deltaij( const int i, const int j )  {return i==j ? 1.0:0.0;}

double sint_p(int i, int j, const dvec & x1, const dvec & x2,
              const dvec & x3, const dvec & csi, double nu);

double sint_u(int i, int j, const dvec & x1, const dvec & x2,
              const dvec & x3, const dvec & csi, double G, double nu);

///////////////////////////////////////////////////////////////////////////////

#endif // SINGULAR_INTEGRALS_H

// End of the file
