#ifndef TOPO_DERIV_H
#define TOPO_DERIV_H

#include <iostream>
#include <vector>
#include <cmath>

typedef std::vector<double> vec;

void topo_deriv( vec & trg_value, vec & deriv, double G, double nu);
void ener_dens( vec & trg_value, vec & dens, double c, double E, double nu);

vec stress2strain(vec & stress, double E, double nu);

#endif // TOPO_DERIV_H
