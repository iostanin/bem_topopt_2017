// This set of functions defines the uniform tension problem
// that is used to test our FMM codes

#ifndef BVP_H
#define BVP_H

#include <iostream>
#include <vector>
#include <cmath>
#include <mpi.h>

using std::cout;
using std::endl;
using std::sqrt;

typedef  std::vector<double> dvec;
typedef  std::vector<int>    ivec;



void generate_vertices(
                        int N,
                        dvec & vert_1,
                        dvec & vert_2,
                        dvec & vert_3,
                        MPI_Comm comm
                      );

// This function sets up a bvp to solve with
// surface solve module

void set_surf_bvp(

        int N,

        // General BVP parameters
        dvec & vert_1,
        dvec & vert_2,
        dvec & vert_3,
        ivec & bc_type,
        dvec & tr_di,
        MPI_Comm comm);


/*
// This function creates input for domain solve

void generate_ds_input_0(
        int N,
        dvec & src_coord, // where force applied
        dvec & surf_coord,   // where disp applied
        dvec & trg_coord, // centers of triangles

        dvec & src_value,  // applied force
        dvec & surf_value,    // applied displacement + normal

        dvec & vert_1,
        dvec & vert_2,
        dvec & vert_3
        );
*/
#endif // BVP_H
