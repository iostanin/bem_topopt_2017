#include "bvp.h"



typedef  std::vector<double> dvec;
typedef  std::vector<int>    ivec;

//=========UNIFORM TENSION TEST PROBLEM==================

// Creates  configuration  of  sources  and  targets that
// correspond to the problem of simple tension on a  cube
// This test fully correspond to "h2 basic bem main" file
// in python project.


// This function generates (-1,1) cube with refinement given by N

void generate_vertices(
                        int N,
                        dvec & vert_1,
                        dvec & vert_2,
                        dvec & vert_3,
                        MPI_Comm comm
                      )
{
        //====================================================

        // Triangle vertices

        //======CUBE EDGE 1 of 6==============================
        for (int i=0; i<N; i++){
            for (int j=0; j<N; j++){
                // ------------- element 1 ------------------
                vert_1[ 3 * (4 * (i + j*N) ) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) ) + 1] = 1.0;
                vert_1[ 3 * (4 * (i + j*N) ) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_2[ 3 * (4 * (i + j*N) ) + 0] = 1.0 - 2.0/N*i;
                vert_2[ 3 * (4 * (i + j*N) ) + 1] = 1.0;
                vert_2[ 3 * (4 * (i + j*N) ) + 2] = - 1.0 + 2.0/N*(j+1);
                vert_3[ 3 * (4 * (i + j*N) ) + 0] = 1.0 - 2.0/N*i;
                vert_3[ 3 * (4 * (i + j*N) ) + 1] = 1.0;
                vert_3[ 3 * (4 * (i + j*N) ) + 2] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------
                //std::cout<<N<<" "<<i<<" "<<j<<" "<<vert_1[ 3 * (4 * (i + j*N) ) + 0]<<std::endl;
                // ------------- element 2 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 1 ) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 1 ) + 1] = 1.0;
                vert_1[ 3 * (4 * (i + j*N) + 1 ) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_2[ 3 * (4 * (i + j*N) + 1 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_2[ 3 * (4 * (i + j*N) + 1 ) + 1] = 1.0;
                vert_2[ 3 * (4 * (i + j*N) + 1 ) + 2] = - 1.0 + 2.0/N*(j+1);
                vert_3[ 3 * (4 * (i + j*N) + 1 ) + 0] = 1.0 - 2.0/N*i;
                vert_3[ 3 * (4 * (i + j*N) + 1 ) + 1] = 1.0;
                vert_3[ 3 * (4 * (i + j*N) + 1 ) + 2] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 3 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 2 ) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 2 ) + 1] = 1.0;
                vert_1[ 3 * (4 * (i + j*N) + 2 ) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_2[ 3 * (4 * (i + j*N) + 2 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_2[ 3 * (4 * (i + j*N) + 2 ) + 1] = 1.0;
                vert_2[ 3 * (4 * (i + j*N) + 2 ) + 2] = - 1.0 + 2.0/N*(j);
                vert_3[ 3 * (4 * (i + j*N) + 2 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_3[ 3 * (4 * (i + j*N) + 2 ) + 1] = 1.0;
                vert_3[ 3 * (4 * (i + j*N) + 2 ) + 2] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 4 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 3 ) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 3 ) + 1] = 1.0;
                vert_1[ 3 * (4 * (i + j*N) + 3 ) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_2[ 3 * (4 * (i + j*N) + 3 ) + 0] = 1.0 - 2.0/N*(i);
                vert_2[ 3 * (4 * (i + j*N) + 3 ) + 1] = 1.0;
                vert_2[ 3 * (4 * (i + j*N) + 3 ) + 2] = - 1.0 + 2.0/N*(j);
                vert_3[ 3 * (4 * (i + j*N) + 3 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_3[ 3 * (4 * (i + j*N) + 3 ) + 1] = 1.0;
                vert_3[ 3 * (4 * (i + j*N) + 3 ) + 2] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------
            }
        }
        //====================================================


        //======CUBE EDGE 2 of 6==============================
        for (int i=0; i<N; i++){
            for (int j=0; j<N; j++){


                // ------------- element 1 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 4*N*N) + 1] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 4*N*N) + 0] = 1.0;
                vert_1[ 3 * (4 * (i + j*N) + 4*N*N) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_3[ 3 * (4 * (i + j*N) + 4*N*N) + 1] = 1.0 - 2.0/N*i;
                vert_3[ 3 * (4 * (i + j*N) + 4*N*N) + 0] = 1.0;
                vert_3[ 3 * (4 * (i + j*N) + 4*N*N) + 2] = - 1.0 + 2.0/N*(j+1);
                vert_2[ 3 * (4 * (i + j*N) + 4*N*N) + 1] = 1.0 - 2.0/N*i;
                vert_2[ 3 * (4 * (i + j*N) + 4*N*N) + 0] = 1.0;
                vert_2[ 3 * (4 * (i + j*N) + 4*N*N) + 2] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------

                // ------------- element 2 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 4*N*N + 1 ) + 1] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 4*N*N + 1 ) + 0] = 1.0;
                vert_1[ 3 * (4 * (i + j*N) + 4*N*N + 1 ) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_3[ 3 * (4 * (i + j*N) + 4*N*N + 1 ) + 1] = 1.0 - 2.0/N*(i+1);
                vert_3[ 3 * (4 * (i + j*N) + 4*N*N + 1 ) + 0] = 1.0;
                vert_3[ 3 * (4 * (i + j*N) + 4*N*N + 1 ) + 2] = - 1.0 + 2.0/N*(j+1);
                vert_2[ 3 * (4 * (i + j*N) + 4*N*N + 1 ) + 1] = 1.0 - 2.0/N*i;
                vert_2[ 3 * (4 * (i + j*N) + 4*N*N + 1 ) + 0] = 1.0;
                vert_2[ 3 * (4 * (i + j*N) + 4*N*N + 1 ) + 2] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 3 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 4*N*N + 2 ) + 1] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 4*N*N + 2 ) + 0] = 1.0;
                vert_1[ 3 * (4 * (i + j*N) + 4*N*N + 2 ) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_3[ 3 * (4 * (i + j*N) + 4*N*N + 2 ) + 1] = 1.0 - 2.0/N*(i+1);
                vert_3[ 3 * (4 * (i + j*N) + 4*N*N + 2 ) + 0] = 1.0;
                vert_3[ 3 * (4 * (i + j*N) + 4*N*N + 2 ) + 2] = - 1.0 + 2.0/N*(j);
                vert_2[ 3 * (4 * (i + j*N) + 4*N*N + 2 ) + 1] = 1.0 - 2.0/N*(i+1);
                vert_2[ 3 * (4 * (i + j*N) + 4*N*N + 2 ) + 0] = 1.0;
                vert_2[ 3 * (4 * (i + j*N) + 4*N*N + 2 ) + 2] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 4 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 4*N*N + 3 ) + 1] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 4*N*N + 3 ) + 0] = 1.0;
                vert_1[ 3 * (4 * (i + j*N) + 4*N*N + 3 ) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_3[ 3 * (4 * (i + j*N) + 4*N*N + 3 ) + 1] = 1.0 - 2.0/N*(i);
                vert_3[ 3 * (4 * (i + j*N) + 4*N*N + 3 ) + 0] = 1.0;
                vert_3[ 3 * (4 * (i + j*N) + 4*N*N + 3 ) + 2] = - 1.0 + 2.0/N*(j);
                vert_2[ 3 * (4 * (i + j*N) + 4*N*N + 3 ) + 1] = 1.0 - 2.0/N*(i+1);
                vert_2[ 3 * (4 * (i + j*N) + 4*N*N + 3 ) + 0] = 1.0;
                vert_2[ 3 * (4 * (i + j*N) + 4*N*N + 3 ) + 2] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------
            }
        }
        //====================================================

        //======CUBE EDGE 3 of 6==============================
        for (int i=0; i<N; i++){
            for (int j=0; j<N; j++){
                // ------------- element 1 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 8*N*N) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 8*N*N) + 1] = -1.0;
                vert_1[ 3 * (4 * (i + j*N) + 8*N*N) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_3[ 3 * (4 * (i + j*N) + 8*N*N) + 0] = 1.0 - 2.0/N*i;
                vert_3[ 3 * (4 * (i + j*N) + 8*N*N) + 1] = -1.0;
                vert_3[ 3 * (4 * (i + j*N) + 8*N*N) + 2] = - 1.0 + 2.0/N*(j+1);
                vert_2[ 3 * (4 * (i + j*N) + 8*N*N) + 0] = 1.0 - 2.0/N*i;
                vert_2[ 3 * (4 * (i + j*N) + 8*N*N) + 1] = -1.0;
                vert_2[ 3 * (4 * (i + j*N) + 8*N*N) + 2] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------

                // ------------- element 2 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 8*N*N + 1 ) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 8*N*N + 1 ) + 1] = -1.0;
                vert_1[ 3 * (4 * (i + j*N) + 8*N*N + 1 ) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_3[ 3 * (4 * (i + j*N) + 8*N*N + 1 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_3[ 3 * (4 * (i + j*N) + 8*N*N + 1 ) + 1] = -1.0;
                vert_3[ 3 * (4 * (i + j*N) + 8*N*N + 1 ) + 2] = - 1.0 + 2.0/N*(j+1);
                vert_2[ 3 * (4 * (i + j*N) + 8*N*N + 1 ) + 0] = 1.0 - 2.0/N*i;
                vert_2[ 3 * (4 * (i + j*N) + 8*N*N + 1 ) + 1] = -1.0;
                vert_2[ 3 * (4 * (i + j*N) + 8*N*N + 1 ) + 2] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 3 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 8*N*N + 2 ) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 8*N*N + 2 ) + 1] = -1.0;
                vert_1[ 3 * (4 * (i + j*N) + 8*N*N + 2 ) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_3[ 3 * (4 * (i + j*N) + 8*N*N + 2 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_3[ 3 * (4 * (i + j*N) + 8*N*N + 2 ) + 1] = -1.0;
                vert_3[ 3 * (4 * (i + j*N) + 8*N*N + 2 ) + 2] = - 1.0 + 2.0/N*(j);
                vert_2[ 3 * (4 * (i + j*N) + 8*N*N + 2 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_2[ 3 * (4 * (i + j*N) + 8*N*N + 2 ) + 1] = -1.0;
                vert_2[ 3 * (4 * (i + j*N) + 8*N*N + 2 ) + 2] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 4 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 8*N*N + 3 ) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 8*N*N + 3 ) + 1] = -1.0;
                vert_1[ 3 * (4 * (i + j*N) + 8*N*N + 3 ) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_3[ 3 * (4 * (i + j*N) + 8*N*N + 3 ) + 0] = 1.0 - 2.0/N*(i);
                vert_3[ 3 * (4 * (i + j*N) + 8*N*N + 3 ) + 1] = -1.0;
                vert_3[ 3 * (4 * (i + j*N) + 8*N*N + 3 ) + 2] = - 1.0 + 2.0/N*(j);
                vert_2[ 3 * (4 * (i + j*N) + 8*N*N + 3 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_2[ 3 * (4 * (i + j*N) + 8*N*N + 3 ) + 1] = -1.0;
                vert_2[ 3 * (4 * (i + j*N) + 8*N*N + 3 ) + 2] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------
            }
        }
        //====================================================

        //======CUBE EDGE 4 of 6==============================
        for (int i=0; i<N; i++){
            for (int j=0; j<N; j++){
                // ------------- element 1 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 12*N*N) + 1] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 12*N*N) + 0] = -1.0;
                vert_1[ 3 * (4 * (i + j*N) + 12*N*N) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_2[ 3 * (4 * (i + j*N) + 12*N*N) + 1] = 1.0 - 2.0/N*i;
                vert_2[ 3 * (4 * (i + j*N) + 12*N*N) + 0] = -1.0;
                vert_2[ 3 * (4 * (i + j*N) + 12*N*N) + 2] = - 1.0 + 2.0/N*(j+1);
                vert_3[ 3 * (4 * (i + j*N) + 12*N*N) + 1] = 1.0 - 2.0/N*i;
                vert_3[ 3 * (4 * (i + j*N) + 12*N*N) + 0] = -1.0;
                vert_3[ 3 * (4 * (i + j*N) + 12*N*N) + 2] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------

                // ------------- element 2 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 12*N*N + 1 ) + 1] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 12*N*N + 1 ) + 0] = -1.0;
                vert_1[ 3 * (4 * (i + j*N) + 12*N*N + 1 ) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_2[ 3 * (4 * (i + j*N) + 12*N*N + 1 ) + 1] = 1.0 - 2.0/N*(i+1);
                vert_2[ 3 * (4 * (i + j*N) + 12*N*N + 1 ) + 0] = -1.0;
                vert_2[ 3 * (4 * (i + j*N) + 12*N*N + 1 ) + 2] = - 1.0 + 2.0/N*(j+1);
                vert_3[ 3 * (4 * (i + j*N) + 12*N*N + 1 ) + 1] = 1.0 - 2.0/N*i;
                vert_3[ 3 * (4 * (i + j*N) + 12*N*N + 1 ) + 0] = -1.0;
                vert_3[ 3 * (4 * (i + j*N) + 12*N*N + 1 ) + 2] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 3 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 12*N*N + 2 ) + 1] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 12*N*N + 2 ) + 0] = -1.0;
                vert_1[ 3 * (4 * (i + j*N) + 12*N*N + 2 ) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_2[ 3 * (4 * (i + j*N) + 12*N*N + 2 ) + 1] = 1.0 - 2.0/N*(i+1);
                vert_2[ 3 * (4 * (i + j*N) + 12*N*N + 2 ) + 0] = -1.0;
                vert_2[ 3 * (4 * (i + j*N) + 12*N*N + 2 ) + 2] = - 1.0 + 2.0/N*(j);
                vert_3[ 3 * (4 * (i + j*N) + 12*N*N + 2 ) + 1] = 1.0 - 2.0/N*(i+1);
                vert_3[ 3 * (4 * (i + j*N) + 12*N*N + 2 ) + 0] = -1.0;
                vert_3[ 3 * (4 * (i + j*N) + 12*N*N + 2 ) + 2] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 4 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 12*N*N + 3 ) + 1] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 12*N*N + 3 ) + 0] = -1.0;
                vert_1[ 3 * (4 * (i + j*N) + 12*N*N + 3 ) + 2] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_2[ 3 * (4 * (i + j*N) + 12*N*N + 3 ) + 1] = 1.0 - 2.0/N*(i);
                vert_2[ 3 * (4 * (i + j*N) + 12*N*N + 3 ) + 0] = -1.0;
                vert_2[ 3 * (4 * (i + j*N) + 12*N*N + 3 ) + 2] = - 1.0 + 2.0/N*(j);
                vert_3[ 3 * (4 * (i + j*N) + 12*N*N + 3 ) + 1] = 1.0 - 2.0/N*(i+1);
                vert_3[ 3 * (4 * (i + j*N) + 12*N*N + 3 ) + 0] = -1.0;
                vert_3[ 3 * (4 * (i + j*N) + 12*N*N + 3 ) + 2] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------
            }
        }
        //====================================================

        //======CUBE EDGE 5 of 6==============================
        for (int i=0; i<N; i++){
            for (int j=0; j<N; j++){
                // ------------- element 1 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 16*N*N) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 16*N*N) + 2] = 1.0;
                vert_1[ 3 * (4 * (i + j*N) + 16*N*N) + 1] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_3[ 3 * (4 * (i + j*N) + 16*N*N) + 0] = 1.0 - 2.0/N*i;
                vert_3[ 3 * (4 * (i + j*N) + 16*N*N) + 2] = 1.0;
                vert_3[ 3 * (4 * (i + j*N) + 16*N*N) + 1] = - 1.0 + 2.0/N*(j+1);
                vert_2[ 3 * (4 * (i + j*N) + 16*N*N) + 0] = 1.0 - 2.0/N*i;
                vert_2[ 3 * (4 * (i + j*N) + 16*N*N) + 2] = 1.0;
                vert_2[ 3 * (4 * (i + j*N) + 16*N*N) + 1] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------

                // ------------- element 2 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 16*N*N + 1 ) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 16*N*N + 1 ) + 2] = 1.0;
                vert_1[ 3 * (4 * (i + j*N) + 16*N*N + 1 ) + 1] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_3[ 3 * (4 * (i + j*N) + 16*N*N + 1 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_3[ 3 * (4 * (i + j*N) + 16*N*N + 1 ) + 2] = 1.0;
                vert_3[ 3 * (4 * (i + j*N) + 16*N*N + 1 ) + 1] = - 1.0 + 2.0/N*(j+1);
                vert_2[ 3 * (4 * (i + j*N) + 16*N*N + 1 ) + 0] = 1.0 - 2.0/N*i;
                vert_2[ 3 * (4 * (i + j*N) + 16*N*N + 1 ) + 2] = 1.0;
                vert_2[ 3 * (4 * (i + j*N) + 16*N*N + 1 ) + 1] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 3 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 16*N*N + 2 ) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 16*N*N + 2 ) + 2] = 1.0;
                vert_1[ 3 * (4 * (i + j*N) + 16*N*N + 2 ) + 1] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_3[ 3 * (4 * (i + j*N) + 16*N*N + 2 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_3[ 3 * (4 * (i + j*N) + 16*N*N + 2 ) + 2] = 1.0;
                vert_3[ 3 * (4 * (i + j*N) + 16*N*N + 2 ) + 1] = - 1.0 + 2.0/N*(j);
                vert_2[ 3 * (4 * (i + j*N) + 16*N*N + 2 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_2[ 3 * (4 * (i + j*N) + 16*N*N + 2 ) + 2] = 1.0;
                vert_2[ 3 * (4 * (i + j*N) + 16*N*N + 2 ) + 1] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 4 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 16*N*N + 3 ) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 16*N*N + 3 ) + 2] = 1.0;
                vert_1[ 3 * (4 * (i + j*N) + 16*N*N + 3 ) + 1] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_3[ 3 * (4 * (i + j*N) + 16*N*N + 3 ) + 0] = 1.0 - 2.0/N*(i);
                vert_3[ 3 * (4 * (i + j*N) + 16*N*N + 3 ) + 2] = 1.0;
                vert_3[ 3 * (4 * (i + j*N) + 16*N*N + 3 ) + 1] = - 1.0 + 2.0/N*(j);
                vert_2[ 3 * (4 * (i + j*N) + 16*N*N + 3 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_2[ 3 * (4 * (i + j*N) + 16*N*N + 3 ) + 2] = 1.0;
                vert_2[ 3 * (4 * (i + j*N) + 16*N*N + 3 ) + 1] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------
            }
        }
        //====================================================

        //======CUBE EDGE 6 of 6==============================
        for (int i=0; i<N; i++){
            for (int j=0; j<N; j++){
                // ------------- element 1 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 20*N*N) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 20*N*N) + 2] = -1.0;
                vert_1[ 3 * (4 * (i + j*N) + 20*N*N) + 1] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_2[ 3 * (4 * (i + j*N) + 20*N*N) + 0] = 1.0 - 2.0/N*i;
                vert_2[ 3 * (4 * (i + j*N) + 20*N*N) + 2] = -1.0;
                vert_2[ 3 * (4 * (i + j*N) + 20*N*N) + 1] = - 1.0 + 2.0/N*(j+1);
                vert_3[ 3 * (4 * (i + j*N) + 20*N*N) + 0] = 1.0 - 2.0/N*i;
                vert_3[ 3 * (4 * (i + j*N) + 20*N*N) + 2] = -1.0;
                vert_3[ 3 * (4 * (i + j*N) + 20*N*N) + 1] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------

                // ------------- element 2 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 20*N*N + 1 ) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 20*N*N + 1 ) + 2] = -1.0;
                vert_1[ 3 * (4 * (i + j*N) + 20*N*N + 1 ) + 1] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_2[ 3 * (4 * (i + j*N) + 20*N*N + 1 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_2[ 3 * (4 * (i + j*N) + 20*N*N + 1 ) + 2] = -1.0;
                vert_2[ 3 * (4 * (i + j*N) + 20*N*N + 1 ) + 1] = - 1.0 + 2.0/N*(j+1);
                vert_3[ 3 * (4 * (i + j*N) + 20*N*N + 1 ) + 0] = 1.0 - 2.0/N*i;
                vert_3[ 3 * (4 * (i + j*N) + 20*N*N + 1 ) + 2] = -1.0;
                vert_3[ 3 * (4 * (i + j*N) + 20*N*N + 1 ) + 1] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 3 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 20*N*N + 2 ) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 20*N*N + 2 ) + 2] = -1.0;
                vert_1[ 3 * (4 * (i + j*N) + 20*N*N + 2 ) + 1] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_2[ 3 * (4 * (i + j*N) + 20*N*N + 2 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_2[ 3 * (4 * (i + j*N) + 20*N*N + 2 ) + 2] = -1.0;
                vert_2[ 3 * (4 * (i + j*N) + 20*N*N + 2 ) + 1] = - 1.0 + 2.0/N*(j);
                vert_3[ 3 * (4 * (i + j*N) + 20*N*N + 2 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_3[ 3 * (4 * (i + j*N) + 20*N*N + 2 ) + 2] = -1.0;
                vert_3[ 3 * (4 * (i + j*N) + 20*N*N + 2 ) + 1] = - 1.0 + 2.0/N*(j+1);
                //-------------------------------------------

                // ------------- element 4 ------------------
                vert_1[ 3 * (4 * (i + j*N) + 20*N*N + 3 ) + 0] = 1.0 - 2.0/N*i - 1.0/N;
                vert_1[ 3 * (4 * (i + j*N) + 20*N*N + 3 ) + 2] = -1.0;
                vert_1[ 3 * (4 * (i + j*N) + 20*N*N + 3 ) + 1] = - 1.0 + 2.0/N*j + 1.0/N;
                vert_2[ 3 * (4 * (i + j*N) + 20*N*N + 3 ) + 0] = 1.0 - 2.0/N*(i);
                vert_2[ 3 * (4 * (i + j*N) + 20*N*N + 3 ) + 2] = -1.0;
                vert_2[ 3 * (4 * (i + j*N) + 20*N*N + 3 ) + 1] = - 1.0 + 2.0/N*(j);
                vert_3[ 3 * (4 * (i + j*N) + 20*N*N + 3 ) + 0] = 1.0 - 2.0/N*(i+1);
                vert_3[ 3 * (4 * (i + j*N) + 20*N*N + 3 ) + 2] = -1.0;
                vert_3[ 3 * (4 * (i + j*N) + 20*N*N + 3 ) + 1] = - 1.0 + 2.0/N*(j);
                //-------------------------------------------
            }
        }

}





//=======================================
//   SURFACE SOLUTION PROBLEM SETUP
//=======================================

void set_surf_bvp(

        int N,

        // General BVP parameters
        dvec & vert_1,
        dvec & vert_2,
        dvec & vert_3,

        ivec & bc_type,

        dvec & tr_di,
        MPI_Comm comm)

{ // Begining of the function

    // Generate triangle vertices
    generate_vertices(N, vert_1, vert_2, vert_3, comm);



    // Transform vertices to (0;1) bounding box
    double margin = 1e-12; //needed to ensure we are within bbox
    for (int i = 0; i<vert_1.size();i++)
    {
        vert_1[i] = (vert_1[i] + 1.0 + margin/2.) / (2.0 + margin);
        vert_2[i] = (vert_2[i] + 1.0 + margin/2.) / (2.0 + margin);
        vert_3[i] = (vert_3[i] + 1.0 + margin/2.) / (2.0 + margin);
    }

    // type of boundary conditions
    // 1 - tractions are given, 2 - displacements are given
    for (int i = 0;      i<20*N*N; i++) bc_type[i] = 1;
    for (int i = 20*N*N; i<24*N*N; i++) bc_type[i] = 2;

    dvec tractions(N * N * 20 * 3);
    dvec displacements(N * N * 4 * 3);

    // tractions
    for (int i = 0; i<20*N*N; i++)
    {for (int k = 0; k<3; k++) tractions[3*i+k] = 0.0;}
    for (int i = 16*N*N; i<20*N*N; i++)
    { tractions[3*i+2] = 1.0;}

    // displacements
    for (int i = 0; i<4*N*N; i++)
    {for (int k = 0; k<3; k++) displacements[3*i+k] = 0.0;}

    // Combined RHS vector
    for (int i = 0; i<20*N*N; i++) {
        for (int k = 0; k<3; k++) {
            tr_di[3*i+k] = tractions[3*i+k];
        }
    }
    const int shift = 20*N*N*3;
    for (int i = 0; i<4*N*N; i++) {
        for (int k = 0; k<3; k++) {
            tr_di[shift + 3*i+k] = displacements[3*i+k];
        }
    }

} // End of function




/*
//======================================================

// Domain solution functions

// Cube test problem with const integration ( 24 * N * N sources )

void generate_ds_input_0(
        int N,
        vec & src_coord, // where force applied
        vec & surf_coord,   // where disp applied
        vec & trg_coord, // centers of triangles

        vec & src_value,  // applied force
        vec & surf_value,    // applied displacement + normal

        vec & vert_1,
        vec & vert_2,
        vec & vert_3
        )
{
    // Generate triangle vertices
    generate_vertices(N, vert_1, vert_2, vert_3);
    double margin = 0.;//10e-9; //needed to ensure we are within bbox
    // Transform vertices to (0;1) bounding box
    for (int i = 0; i<vert_1.size();i++)
    {
        vert_1[i] = (vert_1[i] + 1.0 + margin/2.) / (2.0 + margin);
        vert_2[i] = (vert_2[i] + 1.0 + margin/2.) / (2.0 + margin);
        vert_3[i] = (vert_3[i] + 1.0 + margin/2.) / (2.0 + margin);
    }

    double a1,a2,a3, b1,b2,b3, c1,c2,c3;
    vec norm(24*N*N*3);
    // Compute normals for test purposes
    for (int i = 0; i<24*N*N; i++)
    {
       a1 = vert_2[3*i+0] - vert_1[3*i+0];
       a2 = vert_2[3*i+1] - vert_1[3*i+1];
       a3 = vert_2[3*i+2] - vert_1[3*i+2];

       b1 = vert_3[3*i+0] - vert_1[3*i+0];
       b2 = vert_3[3*i+1] - vert_1[3*i+1];
       b3 = vert_3[3*i+2] - vert_1[3*i+2];

       norm[3*i+0] =  a2*b3-a3*b2;
       norm[3*i+1] =  a3*b1-a1*b3;
       norm[3*i+2] =  a1*b2-a2*b1;

       norm[3*i+0] = norm[3*i+0] / std::sqrt( norm[3*i+0]*norm[3*i+0] + norm[3*i+1]*norm[3*i+1] + norm[3*i+2]*norm[3*i+2]   );
       norm[3*i+1] = norm[3*i+1] / std::sqrt( norm[3*i+0]*norm[3*i+0] + norm[3*i+1]*norm[3*i+1] + norm[3*i+2]*norm[3*i+2]   );
       norm[3*i+2] = norm[3*i+2] / std::sqrt( norm[3*i+0]*norm[3*i+0] + norm[3*i+1]*norm[3*i+1] + norm[3*i+2]*norm[3*i+2]   );
    }


    // Areas of triangles

    vec area(vert_1.size()/3);

    for (int i = 0; i<vert_1.size()/3; i++)
    {
        a1 = vert_2[3*i+0] - vert_1[3*i+0];
        a2 = vert_2[3*i+1] - vert_1[3*i+1];
        a3 = vert_2[3*i+2] - vert_1[3*i+2];

        b1 = vert_3[3*i+0] - vert_1[3*i+0];
        b2 = vert_3[3*i+1] - vert_1[3*i+1];
        b3 = vert_3[3*i+2] - vert_1[3*i+2];

        c1 = a2*b3-a3*b2;
        c2 = a3*b1-a1*b3;
        c3 = a1*b2-a2*b1;

        area[i] = std::sqrt(c1*c1 + c2*c2 + c3*c3) / 2.;
    }

    // Source coordinates
    for (int i = 0; i< 24 * N * N; i++)
    {
        src_coord[ (3*i  )]   = (1.0/3.0)*(vert_1[3*i  ] + vert_2[3*i  ] + vert_3[3*i  ] );
        src_coord[ (3*i+1)]   = (1.0/3.0)*(vert_1[3*i+1] + vert_2[3*i+1] + vert_3[3*i+1] );
        src_coord[ (3*i+2)]   = (1.0/3.0)*(vert_1[3*i+2] + vert_2[3*i+2] + vert_3[3*i+2] );
    }

    // Surf coordinates
   for (int i = 0; i<src_coord.size(); i++) surf_coord[i] = src_coord[i];

    int m = 0;

    // Target coordinates
    for (int i = 0; i<N; i++)
    {
        for (int j = 0; j<N; j++)
        {
            for (int k = 0; k<N; k++)
            {
                trg_coord[3*m+0] = 1.0/N*i+1.0/(2.0*N);
                trg_coord[3*m+1] = 1.0/N*j+1.0/(2.0*N);
                trg_coord[3*m+2] = 1.0/N*k+1.0/(2.0*N);
            m++;
            }
        }
    }

    // Solution in forces (src_value)
    for ( int i = 0; i < src_value.size(); i++ ) src_value[i] = 0;

    for ( int i = 16; i < 20 * N * N; i++ )
    {
        src_value[3 * i + 0] =  0.0;
        src_value[3 * i + 1] =  0.0;
        src_value[3 * i + 2] =  1.0;
    }

    for ( int i = 20; i < 24 * N * N; i++ )
    {
        src_value[3 * i + 0] =  0.0;
        src_value[3 * i + 1] =  0.0;
        src_value[3 * i + 2] = -1.0;
    }

    // Solution in displacements
    // (surf_value)

    for ( int i = 0; i < 24 * N * N; i++ )
    {
        // z displacemens
        surf_value[6 * i + 0] =  0.0;
        surf_value[6 * i + 1] =  0.0;
        surf_value[6 * i + 2] =  surf_coord[3 * i + 2];
    }

    // normals
    for ( int i = 0; i < 24 * N * N; i++ )
    {   // x+ normal
        surf_value[6 * i + 3] =  norm[3*i];
        surf_value[6 * i + 4] =  norm[3*i+1];
        surf_value[6 * i + 5] =  norm[3*i+2];    }


    // Assign quad weights

    for (int i = 0; i < 24 * N * N; i++)
    {
        for (int j=0; j<3; j++)
        {
             src_value[3 * i + j] *= area[i];
            surf_value[6 * i + j] *= area[i];
        }
    }

    // BVP is set
}
*/







