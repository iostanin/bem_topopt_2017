-include $(PVFMM_DIR)/MakeVariables

ifndef CXX_PVFMM
-include ../MakeVariables
endif

ifndef CXXFLAGS_PVFMM
$(error Cannot find file: MakeVariables)
endif

CXX=$(CXX_PVFMM)
CXXFLAGS=$(CXXFLAGS_PVFMM)
LDLIBS=$(LDLIBS_PVFMM)

CXXFLAGS += -std=c++0x -I/opt/cluster/math/petsc/psxe_2015.6/i4/include
LDLIBS += -L/opt/cluster/math/petsc/psxe_2015.6/i4/lib -lpetsc

RM = rm -f
MKDIRS = mkdir -p

BINDIR = ./bin
SRCDIR = ./src
OBJDIR = ./obj
INCDIR = ./include

TARGET_BIN = $(BINDIR)/test_solver_ksp

DOMAIN_SRC = bvp.cpp \
             matvec.cpp \
             singular_integrals.cpp \
             elastic_kernels.cpp \
             fmm_summation.cpp \
             auxillary.cpp \
             topo_deriv.cpp \
             mesher.cpp \
             solver_ksp.cpp \
             stl_io.cpp \
             main.cpp



DOMAIN_DIR_OBJ = $(patsubst %.cpp, $(OBJDIR)/%.o, $(DOMAIN_SRC))

#DOMAIN_DIR_OBJ = $(subst .cpp,.o,$(DOMAIN_DIR_SRC))


#test:
#	@echo $(DOMAIN_DIR_OBJ)

all : $(TARGET_BIN)

#ifeq ($(INTEL_OFFLOAD_OK),yes)

#$(BINDIR)/%: $(OBJDIR)/%.o
#	-@$(MKDIRS) $(dir $@)
#	$(CXX) $(CXXFLAGS) -no-offload         $^       $(LDLIBS) -o $@
#	$(CXX) $(CXXFLAGS)                     $^_async $(LDLIBS) -o $@_async
#	$(CXX) $(CXXFLAGS) -D__DEVICE_SYNC__=1 $^_mic   $(LDLIBS) -o $@_mic

#$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
#	-@$(MKDIRS) $(dir $@)
#	$(CXX) $(CXXFLAGS) -no-offload         -I$(INCDIR) -c $^ -o $@
#	$(CXX) $(CXXFLAGS)                     -I$(INCDIR) -c $^ -o $@_async
#	$(CXX) $(CXXFLAGS) -D__DEVICE_SYNC__=1 -I$(INCDIR) -c $^ -o $@_mic

#else

$(TARGET_BIN): $(DOMAIN_DIR_OBJ)
	-@$(MKDIRS) $(dir $@)
	$(CXX) $(CXXFLAGS) $^ $(LDLIBS) -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	@echo "  ---  custom compilation  ---  "
	-@$(MKDIRS) $(dir $@)
	$(CXX) $(CXXFLAGS) -I$(INCDIR) -c $^ -o $@

#endif

clean:
	$(RM) -r $(BINDIR)/* $(OBJDIR)/*
	$(RM) *~ */*~

